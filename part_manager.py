from tkinter import *


#functions
def populate_list():
    print('populate')

def add_item():
    print('add')

def update_item():
    print('update')

def remove_item():
    print('remove')

def clear_item():
    print('clear')

# creating new window object
app = Tk()

# part design
app.title('part manager')
app.geometry('700x500')

part_text = StringVar()
part_label = Label(app, text='part name', font=('bold', 14), pady=20)
part_label.grid(row=0, column=0, sticky=W)
part_entry = Entry(app, textvariable=part_text)
part_entry.grid(row=0, column=1)

customer_text = StringVar()
customer_label = Label(app, text='customer', font=('bold', 14), pady=20)
customer_label.grid(row=0, column=2, sticky=E)
customer_entry = Entry(app, textvariable=customer_text)
customer_entry.grid(row=0, column=3)

retailer_text = StringVar()
retailer_label = Label(app, text='retailer', font=('bold', 14), pady=20)
retailer_label.grid(row=1, column=0, sticky=W)
retailer_entry = Entry(app, textvariable=retailer_text)
retailer_entry.grid(row=1, column=1)

price_text = int()
price_label = Label(app, text='Price', font=('bold', 14), pady=20)
price_label.grid(row=1, column=2,padx=(50,20), sticky=W)
price_entry = Entry(app, textvariable=price_text)
price_entry.grid(row=1, column=3)

#creating parts list(listbox)
parts_list= Listbox(app, height=8, width= 75, border = 0)
parts_list.grid(row=3,column=0,columnspan=6, rowspan=3, pady=6,padx=20)

#scrollbar
scrollbar=Scrollbar(app)
scrollbar.grid(row=3,column=4)

#setting the scrollbar tothe list box
parts_list.configure(yscrollcommand=scrollbar.set)
scrollbar.configure(command=parts_list.yview)

#buttons
add_btn=Button(app, text='add part', width=10, command=add_item)
add_btn.grid(row=2, column=0, pady=20,padx=(35,10))

remove_btn=Button(app, text='remove part', width=10, command=remove_item)
remove_btn.grid(row=2, column=1, pady=20,padx=10)

update_btn=Button(app, text='update part', width=10, command=update_item)
update_btn.grid(row=2, column=2, pady=20, padx=10)

clear_btn=Button(app, text='clear part', width=10, command=clear_item)
clear_btn.grid(row=2, column=3, pady=20, padx=10)

#populate data
populate_list()
# starting program
app.mainloop()
